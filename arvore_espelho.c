// O espelho de uma árvore binária T, esp(T), é a árvore binária definida recursivamente da seguinte forma:

// - Se T for vazia então esp(T) é a árvore vazia;
// - Se T tem raiz Tr, subárvore da esqueda Te e suárvore da direita Td, então esp(T) é a árvore binária com raiz Tr, subárvore esquerda esp(Td) e subárvore direita esp(Te).


#include <stdio.h>
#include <stdlib.h>

typedef struct Arvore {
    int valor;
    struct Arvore *esquerda;
    struct Arvore *direita;
} Arvore;

// Função auxiliar para criar um novo nó de árvore
Arvore *criar_no(int valor) {
    Arvore *novo_no = (Arvore *)malloc(sizeof(Arvore));
    novo_no->valor = valor;
    novo_no->esquerda = NULL;
    novo_no->direita = NULL;
    return novo_no;
}

// Função para verificar se duas árvores são espelhos entre si
int eh_espelho(Arvore *arv_a, Arvore *arv_b) {
    // Se ambas as árvores são vazias, elas são espelhos entre si
    if (arv_a == NULL && arv_b == NULL) {
        return 1;
    }
    // Se uma das árvores for vazia e a outra não, elas não são espelhos
    if (arv_a == NULL || arv_b == NULL) {
        return 0;
    }
    // Verifica se os valores dos nós correspondentes são iguais e
    return (arv_a->valor == arv_b->valor) &&
           eh_espelho(arv_a->esquerda, arv_b->direita) &&
           eh_espelho(arv_a->direita, arv_b->esquerda);
}

// Função para criar uma árvore espelho de uma árvore dada
Arvore *cria_espelho(Arvore *arv_a) {
    if (arv_a == NULL) {
        return NULL;
    }

    Arvore *novo_no = criar_no(arv_a->valor);

    // Recursivamente cria subárvores espelho
    novo_no->esquerda = cria_espelho(arv_a->direita);
    novo_no->direita = cria_espelho(arv_a->esquerda);

    return novo_no;
}

int main() {
    Arvore *arv_a = criar_no(1);
    arv_a->esquerda = criar_no(2);
    arv_a->direita = criar_no(3);

    Arvore *arv_b = criar_no(1);
    arv_b->esquerda = criar_no(3);
    arv_b->direita = criar_no(2);

    if (eh_espelho(arv_a, arv_b)) {
        printf("As arvores sao espelhos entre si.\n");
    } else {
        printf("As arvores nao sao espelhos entre si.\n");
    }

    Arvore *arv_espelho = cria_espelho(arv_a);
    // Agora arv_espelho é uma cópia espelho de arv_a
    return 0;
}
